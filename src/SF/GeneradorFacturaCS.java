/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SF;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author DD
 */
public class GeneradorFacturaCS implements IFactura{
    private static GeneradorFacturaCS generador;
    private Pdf pdf;
    
    private GeneradorFacturaCS(){
    }
    
    public static GeneradorFacturaCS obtenerInstancia(){
        if(generador==null){
            generador=new GeneradorFacturaCS();
        }
        return generador;
    }
    
    public String obtenerNombre(String idCaja) {
    	Date date = new Date();
    	DateFormat hourdateFormat = new SimpleDateFormat("HH-mm-ss dd-MM-yyyy");
    	return  idCaja+ "=="+hourdateFormat.format(date);
    	
    }
    @Override
    public void crearFactura(String[] datos){
        pdf= new Pdf(this.obtenerNombre(datos[0]));
        pdf.open();
        pdf.writeTitle("DCNKRTC.com");
        pdf.writeParagraph("Caja            :        "+datos[0]);
        pdf.writeParagraph("Clave           :        "+datos[1]);
        pdf.writeParagraph("Tiempo          :        "+datos[2]);
        pdf.writeParagraph("Valor servicio  :        "+datos[3]);
        pdf.close();
    }
   
    
}
