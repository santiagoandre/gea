/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SF;

/**
 *
 * @author DD
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;





public class Pdf{
	private Document document;
    private String name;
	private static Font paragraphFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
	            Font.NORMAL, BaseColor.BLACK);
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 20,
	            Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 15,
	            Font.BOLD);
	public Pdf(String name)  {
            this.name=name;
            FileOutputStream file;
            try {
            		String nombre = "Recibos/"+name;
            		nombre = crearArchivo(nombre,"pdf");
                    file = new FileOutputStream(nombre,false);
                    
                    document = new Document();
                    PdfWriter.getInstance(document,file);
            } catch (Exception e) {
                    e.printStackTrace();
            } 
	}

	private String crearArchivo(String nombre,String extencion) {
		File f = new File(nombre+"."+extencion);
		int i = 1;
		while(f.exists()){
			nombre  =  nombre+i;
			i++;
			f =new File(nombre+"."+extencion);
		}
		try {
			f.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  nombre+"."+extencion;
	}

	void open() {
            if(document != null)
                    document.open();
	}

	void close() {
            if(document != null){
                    document.close();
                    document = null;
            }
	}

	
	void writeParagraph(String text) {
            Paragraph p = new Paragraph(text);
            try {
                    document.add(p);
            } catch (DocumentException e) {

            }
	}

	
	void addPage() {
            document.newPage();
	}

	
	void writeTitle(String title) {
            Paragraph preface = new Paragraph();
            // We add one empty line
            addEmptyLine(preface, 1);
            // Lets write a big header
            preface.add(new Paragraph(title, titleFont));

            preface.setAlignment(Element.ALIGN_CENTER);
            addEmptyLine(preface, 1);
      

            try {
		document.add(preface);
	    } catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}

	}


	
	void writeSubtitle(String text) {
		Paragraph preface = new Paragraph();
        preface.add(new Paragraph(text, smallBold));



        try {
			document.add(preface);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
        public static void addEmptyLine(Paragraph paragraph, int number) {
            for (int i = 0; i < number; i++) {
                paragraph.add(new Paragraph(" "));
            }
        }
	

}

