package SRS;

import SCCS.model.SistemaServicio;

public abstract class RespaldoArchivo implements IRespaldoSistema{
	protected SistemaServicio sistema;
	protected String path;
	public RespaldoArchivo( String path,SistemaServicio sistema) {
		
		
		this.sistema = sistema;
		this.path= path;
	}
	
	
	public SistemaServicio getSistema() {
		return sistema;
	}
	
	

}
