package SRS;

public interface IRespaldoSistema {
//una clase que hace el respald de un sistema de servicios
	public abstract  void push();
	public abstract  void pull();
}
