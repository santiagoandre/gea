package SRS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import SCCS.model.SistemaCajasDeSeguridad;
import SCCS.model.SistemaServicio;
import SCS.model.CajaDeSeguridad;
import SCS.model.Servicio;
import SCS.model.TarifaTiempo;
import SCS.model.SistemasCaja.AbsSistemaDesbloqueo;
import SCS.model.SistemasCaja.SistemaDesbloqueo;
import SistemaArchivo.Archivo;

public class RespaldoSistemaCaja extends RespaldoArchivo{

		private SimpleDateFormat formatoFechas = new SimpleDateFormat("yyyy-MM-dd=HH:mm:ss");
	public RespaldoSistemaCaja(String path,SistemaServicio sistema) {
		super(path,sistema);
	}

	@Override
	public void push() {
		//id estado
				//si no esta libre:
					//fechaInicio tiempoUsar costo codigoDesbloqueo
		List<Servicio> servicios =  getSistema().getServicios();
		Archivo a =crearArchivo();
		a.abrirArchivo(path);
		for(Servicio servicio : servicios){
			CajaDeSeguridad caja = (CajaDeSeguridad) servicio;
			String id = caja.getId();
			int estado = caja.getEstado();
			String infoCaja = id+" "+estado+" ";
			if(estado!=caja.LIBRE)
				infoCaja = infoCaja + getInfoCajaOcupada(caja);
			infoCaja = infoCaja+"\n";
			a.escribir(infoCaja);
		}
		a.close();
		
	}

	@Override
	public void pull() {
		//cargar Sistema
		Archivo a =crearArchivo();
		a.abrirArchivo(path);
		String strCaja = a.Leer();
		while(strCaja != null){
			CajaDeSeguridad caja = crearCaja(strCaja);
			getSistema().addServicio(caja);
			strCaja = a.Leer();
		}
		a.close();
	}
	
	
	

	private CajaDeSeguridad crearCaja(String strCaja) {
		String atributos[]=strCaja.split(" ");
		String id = (atributos[0]);
		int estado = Integer.parseInt(atributos[1]);
		
		CajaDeSeguridad caja = new CajaDeSeguridad(id,estado);
		if(estado == caja.LIBRE)
			return caja;
		
		String fechaInicio = atributos[2];
		int tiempoUso = Integer.parseInt(atributos[3]);
		int costo = Integer.parseInt(atributos[4]);
		int codigoDesbloqueo = Integer.parseInt(atributos[5]);
		
		caja.setFechaInicio(strToDate(fechaInicio));
		System.out.println("fecha inicio cargada: " +caja.getFechaInicio());
		caja.setTiempoUso(tiempoUso);
		caja.seleccionarTarifa(new TarifaTiempo(tiempoUso,costo));
		AbsSistemaDesbloqueo s = caja.getSistemaDesbloqueo();
		s.setCodigoDesbloqueoUsuario(codigoDesbloqueo);
		return caja;
	}

	private Date strToDate(String fechaInicio) {
		try {
			return formatoFechas.parse(fechaInicio);
		} catch (ParseException e) {
			return null;
		}
		
	}

	private String getInfoCajaOcupada(CajaDeSeguridad caja) {
		String fechaInicio = formatoFechas.format(caja.getFechaInicio());
		int tiempoUso = caja.getTiempoUso();
		int costo = caja.getTarifaSeleccionada().getCosto();
		int codigoDesbloqueo = caja.getSistemaDesbloqueo().getCodigoDesbloqueoUsuario();
		return fechaInicio+ " "
			+ Integer.toString(tiempoUso) + " "
			+ Integer.toString(costo) + " "
			+ Integer.toString(codigoDesbloqueo);
	}

	private Archivo crearArchivo() {
		return new Archivo();
	}
}
