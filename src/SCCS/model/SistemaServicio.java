package SCCS.model;

import java.util.List;

import SCS.model.Servicio;
import SRS.IRespaldoSistema;

public abstract class SistemaServicio extends mvca.Model {
	protected List<Servicio> servicios;
	protected IRespaldoSistema sistemaRespaldo;
	public List<Servicio> getServicios(){
		return servicios;
	}
	public void addServicio(Servicio s){
		//if(s.type =this.type) 
		        //principio de liskov
				//colocar resticciones en el alto nivel
				//si sitemaServicio solo puede tener un tipo de servicio
			servicios.add(s);	
	}
	protected abstract void fabricaSistemaRespaldo();

}
