package SCCS.model;

import java.util.ArrayList;

import SCS.model.CajaDeSeguridad;
import SCS.model.Servicio;
import SRS.IRespaldoSistema;
import SRS.RespaldoSistemaCaja;

public class SistemaCajasDeSeguridad extends SistemaServicio {
	
	public SistemaCajasDeSeguridad(){
		servicios = new ArrayList<>();
		fabricaSistemaRespaldo();
	}
	public boolean eliminarCaja(int id){
		//falta
		return false;
	}
	public void cargarCajasSeguridad(){
		this.sistemaRespaldo.pull();
		notificar();
	}
	public void guardarEstadoSistema() {
		this.sistemaRespaldo.push();		
	}
	@Override
	protected void fabricaSistemaRespaldo() {
		this.sistemaRespaldo = new RespaldoSistemaCaja("Respaldo/SCS.txt",this);
	}
	
	
}
