/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SCCS.view;

import java.awt.GridLayout;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.JFrame;

import mvca.Model;
import SCCS.model.SistemaCajasDeSeguridad;
import SCS.Controller.CajaSeguridadController;
import SCS.Controller.DesocuparCajaSeguridadController;
import SCS.Controller.OcuparCajaSeguridadController;
import SCS.Controller.TarifasController;
import SCS.model.CajaDeSeguridad;
import SCS.model.Servicio;
import SCS.view.VistaCajaSeguridad;
import SCS.view.VistaDesocuparCajaSeguridad;
import SCS.view.VistaOcuparCajaSeguridad;

/**
 *
 * @author DD
 */
public class VCajasDeSeguridad extends JFrame implements mvca.View{
    
    
    public VCajasDeSeguridad(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("DCNKRTC.com");
    }
    

     
    public VistaCajaSeguridad asociarVistaCaja(CajaDeSeguridad caja){
        //creo las vistas
        VistaDesocuparCajaSeguridad vDCajaSeguridad= new VistaDesocuparCajaSeguridad();
        VistaOcuparCajaSeguridad vOCajaSeguridad= new VistaOcuparCajaSeguridad();
         VistaCajaSeguridad vCajaSeguridad= new VistaCajaSeguridad(vDCajaSeguridad,vOCajaSeguridad);
        //creo los controllers y los asocio a la s vistas y el modelo
        TarifasController tController= new TarifasController(caja,vOCajaSeguridad);
        CajaSeguridadController cCajaSeguridad =  new CajaSeguridadController(caja,vCajaSeguridad);
        OcuparCajaSeguridadController cOCajaSeguridad =  new OcuparCajaSeguridadController(caja,vOCajaSeguridad);
        DesocuparCajaSeguridadController cDCajaSeguridad =  new DesocuparCajaSeguridadController(caja,vDCajaSeguridad);
        //asocio el modelo a la vista principal
        caja.addView(vCajaSeguridad);
        //anado los controllers a las vistas
        vCajaSeguridad.setController(cCajaSeguridad);
        vOCajaSeguridad.setControllerAL(cOCajaSeguridad);
        vOCajaSeguridad.setControllerIL(tController);//no encontre otra manera
        vDCajaSeguridad.setController(cDCajaSeguridad);
        //retorno la vista principal
        return vCajaSeguridad;
    }

	@Override
	public void actualizar(Model aModel) {
		//obtengo el modeloSistema
		SistemaCajasDeSeguridad sistema =  (SistemaCajasDeSeguridad) aModel;
		//obtengo las cajas que hay en el sistema
		List<Servicio> modeloCajas = sistema.getServicios();
		//con ese numero ya se que tamanio debe tener la interfaz
		cambiarDimenciones(modeloCajas.size());
        for (Servicio caja: modeloCajas){
        	//asocio una interfaz grafica con la caja y la aniado
        	VistaCajaSeguridad view =asociarVistaCaja((CajaDeSeguridad) caja);
            
        	this.add(view);
        }
		
	}

	private void cambiarDimenciones(int nCajas) {
		//se crea un cuadro de M*N = nCajas
		int M = obtenerAturaGabinete(nCajas);
		int N = nCajas/M;


        this.fixSize(M,N);
       // this.setSize(M*200+40, N*151+40);
        this.setLayout(new GridLayout(M,N,10,10));
		
	}
    
	private int obtenerAturaGabinete(int nCajas){
		if(nCajas ==1 )
			return 1;
			
		int i = 2;
		while(nCajas%i != 0)
			i++;
		return i;
	}
	//Fija el tamaño del formulario
    private void fixSize(int nHighBox,int nWidthBox){
        int High = 50+nHighBox*200;//200 es el alto de la imagen
        int Width= 50+nWidthBox*151;//151 es el ancho de la imagen
        this.setBounds(0, 0, Width, High);
        this.setResizable(false);
    }

	@Override
	public void notificarMensaje(Model model, String mensaje) {
		System.out.println(mensaje);
		
	}
	public void setController(mvca.Controller c){
		this.addWindowListener((WindowListener) c);
	}
}
