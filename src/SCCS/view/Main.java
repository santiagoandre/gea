package SCCS.view;

import java.awt.EventQueue;

import SCCS.controller.PersistenciaController;
import SCCS.model.SistemaCajasDeSeguridad;

public class Main {

	 //Inicio de la aplicación.
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    loadMVCSistemaCajasDeSeguridad();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

			
        });
    }
    private static void loadMVCSistemaCajasDeSeguridad() {
		VCajasDeSeguridad frame = new VCajasDeSeguridad();
		SistemaCajasDeSeguridad sistema = new SistemaCajasDeSeguridad();
		PersistenciaController pcller = new PersistenciaController(sistema,frame);
		frame.setController(pcller);
		sistema.addView(frame);
		sistema.cargarCajasSeguridad();
		
        frame.setVisible(true);
	}

}
