package SCCS.controller;

import SCCS.model.SistemaCajasDeSeguridad;
import SCCS.view.VCajasDeSeguridad;
import AbsControllers.WindowsController;

public class PersistenciaController extends WindowsController {
	private SistemaCajasDeSeguridad sistema;
	private VCajasDeSeguridad view;

	public PersistenciaController( SistemaCajasDeSeguridad sistema, VCajasDeSeguridad view){
		this.view=view;
		this.sistema=sistema;
	}
	@Override
	protected boolean isValid() {
		return true;
	}

	@Override
	public void actualizar() {
		sistema.guardarEstadoSistema();
		view.setVisible(false);
		view.dispose();
	}

}
