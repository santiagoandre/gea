package mvca;

import mvca.Model;

public interface  View{


    public void actualizar(Model aModel);

	public void notificarMensaje(Model model, String mensaje);

}

