package mvca;

import java.util.ArrayList;


/**
 *
 * @author pmage
 */
public class Model {
    
     private ArrayList<View> views = new ArrayList<View>();
     private View sourceView;
    public void addView(View view) {
        views.add(view);
    }

    public ArrayList<View> getViews() {
        return views;
    }
    
    public void notificar(){
        
         for (View view : views) {
             if(this.sourceView!=view)
                   view.actualizar(this);
         }
    }

	public void notificar(String mensaje) {
		 
         for (View view : views) {
             if(this.sourceView!=view)
                   view.notificarMensaje(this,mensaje);
         }
	}
    /**
     * @return the sourceView
    */
    public View getSourceView() {
        return sourceView;
    }

    /**
     * @param sourceView the sourceView to set
    */
    public void setSourceView(View sourceView) {
        this.sourceView = sourceView;
    }
    

    
}
