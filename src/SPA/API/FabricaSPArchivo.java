package SPA.API;

import SPA.controller.CerrarSistemaController;


import SPA.controller.CredencialesPagoController;
import SPA.controller.MetodosDePagoController;
import SPA.model.AbsApp;
import SPA.model.App;
import SPA.view.FormCredencialesPago;
import SPA.view.VMetodosDePago;

public class FabricaSPArchivo implements IFabricaSistemaPago{
	
	@Override
	public ICobrable getInstance() {
		return generarSistema();
	}
	private AbsApp generarSistema(){
		//creo el modelo
		AbsApp app = new App();
		//creo las vistas
		VMetodosDePago interfazSistema= new VMetodosDePago();
		FormCredencialesPago fromCredenciales = interfazSistema.getFormCredenciales();
		//creo los controllers y los asocio al modelo y sus vistas
		CredencialesPagoController pagocller = new CredencialesPagoController(fromCredenciales,app);
		MetodosDePagoController metodospagocller = new MetodosDePagoController(interfazSistema,app);
		CerrarSistemaController cerrarcller = new CerrarSistemaController(app,interfazSistema);
		//asocio la vista principal al modelo		
		app.addView(interfazSistema);
		//asocio los controllers con las vistas
		fromCredenciales.setController(pagocller);
		interfazSistema.setControllerCbx(metodospagocller);
		interfazSistema.setControllerWindow(cerrarcller);
		//llamo el metodo actualizar para la vista por defecto 
		interfazSistema.actualizar(app);
		metodospagocller.actualizar();
		return app;
	}
}
