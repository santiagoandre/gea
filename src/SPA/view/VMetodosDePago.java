package SPA.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ItemListener;
import java.awt.event.WindowListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;

import SPA.controller.CredencialesPagoController;
import SPA.controller.MetodosDePagoController;
import SPA.model.App;
import mvca.Controller;
import mvca.Model;
import mvca.View;


public class VMetodosDePago extends JFrame implements View{

	private JPanel contentPane;
	private FormCredencialesPago FromCredenciales;
	private JComboBox cbxMetodosDePago;
	public VMetodosDePago() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		cbxMetodosDePago = new JComboBox();
		contentPane.add(cbxMetodosDePago, BorderLayout.NORTH);
		FromCredenciales = new FormCredencialesPago(this);
		contentPane.add(FromCredenciales, BorderLayout.SOUTH);

	
	}
   

	@Override
	public void actualizar(Model aModel) {

		//se cargan los metodos de pago en el combobox
		App app = (App) aModel;
		if(app.isSuccess())
			this.setVisible(false);
		else if(!this.isVisible())
			this.setVisible(true);			
		Object[] metodos =  app.getTiposMetodosDePago().toArray();
		DefaultComboBoxModel  cbxModel = new DefaultComboBoxModel (metodos);
		this.cbxMetodosDePago.setModel(cbxModel);
		//this.setBounds(getMaximizedBounds());
		
	}

	@Override
	public void notificarMensaje(Model model, String mensaje) {
		// mostrar un mensaje en la interfaz
		//falta
		System.out.println(mensaje);
		
	}
	public void setControllerCbx(Controller c){
		this.cbxMetodosDePago.addItemListener((ItemListener) c);
	}

	public void setControllerWindow(Controller c){
		this.addWindowListener((WindowListener) c);
	}

	public String getMetodoPago() {
		return this.cbxMetodosDePago.getSelectedItem().toString();
	}

	public FormCredencialesPago getFormCredenciales() {
		return this.FromCredenciales;
	}


	
}
