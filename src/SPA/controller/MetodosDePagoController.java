package SPA.controller;



import AbsControllers.ItemChangeController;
import SPA.model.App;
import SPA.view.FormCredencialesPago;
import SPA.view.VMetodosDePago;
import mvca.Model;
import mvca.View;


public class MetodosDePagoController extends ItemChangeController

{

	private VMetodosDePago view;
	private App app;
	
	public MetodosDePagoController(View aView, Model aModel) {
		super();
        view = (VMetodosDePago) aView;
        app = (App) aModel;

	}
	@Override
	public void actualizar() {
		
		String metodoPago = view.getMetodoPago();
		app.seleccionarMetodoPago(metodoPago);
		
		FormCredencialesPago from = view.getFormCredenciales();
		from.actualizar(app, metodoPago);
		
	}

	@Override
	protected boolean isValid() {
		return !view.getMetodoPago().isEmpty();
	}
}

