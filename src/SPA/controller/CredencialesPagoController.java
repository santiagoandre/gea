package SPA.controller;


import java.util.ArrayList;



import AbsControllers.ButtonController;
import SPA.model.App;
import SPA.view.FormCredencialesPago;
import mvca.Model;



public class CredencialesPagoController extends ButtonController

{
	private FormCredencialesPago view;
	private App app;
	public CredencialesPagoController(FormCredencialesPago aView, Model aModel) {
		super();
        view = aView;
        app = (App) aModel;

	}
	@Override
	public void actualizar() {
		ArrayList<String> credenciales = view.getCredenciales();
		app.cargarCredenciales(credenciales);
		app.iniciar();
	}

	@Override
	protected boolean isValid() {
		return view.esValido();
		
	}


}

