package SPA.controller;

import AbsControllers.WindowsController;
import SPA.model.AbsApp;
import SPA.view.VMetodosDePago;

public class CerrarSistemaController extends WindowsController {
	private AbsApp app;
	private VMetodosDePago view;

	public CerrarSistemaController(AbsApp app, VMetodosDePago view){
		this.app = app;
		this.view = view;
	}
	@Override
	protected boolean isValid() {
		return true;
	}

	@Override
	public void actualizar() {
		app.abortar();
		view.setVisible(false);
	}

}
