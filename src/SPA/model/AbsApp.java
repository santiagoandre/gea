package SPA.model;

import java.util.HashMap;
import java.util.List;

import SPA.API.IClienteSistemaPago;
import SPA.API.ICobrable;
import mvca.Model;

public abstract class AbsApp extends Model implements ICobrable

{	
	private boolean estado;//exito o no
	protected ServicioPago servicio;

	//atributos del pago 
	private int cantidad;
	protected String metodoPago;
	private List credenciales;
	private IClienteSistemaPago cliente;
	@Override
	public void cobrar(int cantidad,IClienteSistemaPago cliente){
		this.cantidad = cantidad;
		this.cliente = cliente;
		//lanzar
	}
	public void lanzar(){
		//muestra las vistas asociadas a este modelo
		notificar();
	}


	public boolean isSuccess(){
		return estado;
	}
	public void iniciar() {
		crearServicio();
		int respuesta = servicio.cobrar(cantidad, credenciales);
		switch(respuesta)
		{
			case ServicioPago.SUCCESS:
				//exito, hay que decirle a a vista que muestre ese mensaje y termine
				notificar("Exito, paga realizado.");
				estado = true;
				this.cliente.notificar(this);
				break;
			case ServicioPago.INSUFFICIENT_BALANCE:
				//exito, hay que decirle a a vista que muestre ese mensaje y termine
				notificar("Error: Saldo insuficiente.");
				estado = false;
				break;
			case ServicioPago.UNAUTHENTICATED:
				//exito, hay que decirle a a vista que muestre ese mensaje y termine
				notificar("Error: credenciales invalidas.");
				estado = false;
				break;
			case ServicioPago.ERROR:
				notificar("Error: Ah ocurrido un errror en el sistema, contacte al aministrador");
				estado = false;
				break;
			}
		notificar();
	}
				
	public void seleccionarMetodoPago(String metodo){
		this.metodoPago = metodo;
	}
	public void cargarCredenciales(List credenciales){
		this.credenciales = credenciales;
	}
		
	public void abortar() {
		//this.estado = false;
		this.cliente.notificar(this);				
	}

	public HashMap<String, Class> getCredencialesNecesarias() {
		
		crearServicio();
		return servicio.getCredencialesNecesarias();
		
	}

	public abstract List getTiposMetodosDePago();//devuele una lista con los metodos de pago disponibles.
	
	public abstract void crearServicio();//metodo fabrica
}
