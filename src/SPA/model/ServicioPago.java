package SPA.model;

import java.util.HashMap;
import java.util.List;


public abstract class ServicioPago
{
	public static final int SUCCESS = 0; //exito se realizo el pago
	public static final int UNAUTHENTICATED = 1; //no se autentifico el usuario,
	public static final int INSUFFICIENT_BALANCE = 2; //saldo insuficiente
	public static final int ERROR = 3; //algun otro error
	protected abstract boolean validar(List credenciales);//valida las credenciales del metodo de pago
	protected abstract boolean validarSaldo(int saldominimo);//valida si el saldo es suficiente
	protected abstract boolean actualizarSaldo(int cantidadCobrar); //cobra, sabiendo 
	protected int cobrar(int cantidad,List credenciales){
		try{
			if (!validar(credenciales))
				return UNAUTHENTICATED;
			if(!validarSaldo(cantidad))
				return INSUFFICIENT_BALANCE;
			if (actualizarSaldo(cantidad))
				return ERROR;
		}catch(Exception e){
			return  ERROR;
		}
		return SUCCESS;	
	}
	public abstract HashMap<String, Class> getCredencialesNecesarias();
	
	
}

