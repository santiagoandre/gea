package SistemaArchivo;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;

public class Archivo {
		private String nombre;
		private BufferedWriter texto;
		private BufferedReader lectura;
	
	public Archivo()
	{	
	}
	
	public void abrirArchivo(String nombreArchivo)
	{
		nombre = nombreArchivo;
	}
	
	private void alistarLector(){
		try {
			lectura =new BufferedReader(new FileReader(nombre));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//
	}
	private void alistarEscritor(){
		try {
			texto = new BufferedWriter(new FileWriter(nombre));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String Leer()
	{	
		if(lectura == null)
			alistarLector();

		try {
			return lectura.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public void cambiarLinea(int numero, String nuevaLinea) {
		Archivo a  = new Archivo();
		a.abrirArchivo(nombre);
		String linea = a.Leer();
		String contenido = "";
		int i = 0;
		while(linea!= null){
			if(i==numero)
				contenido = contenido + nuevaLinea+ '\n';
			else
				contenido = contenido + linea +'\n';
			linea = a.Leer();
			i++;
		}
		a.escribir(contenido);
		a.close();
		
	}
	public void escribir(String entrada){
		if(texto == null)
			alistarEscritor();
		try {
			texto.write(entrada);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void close(){
		try {
			if(texto!=null)
				texto.close();
			if(lectura!=null)
				lectura.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
	