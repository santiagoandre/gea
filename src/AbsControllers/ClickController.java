package AbsControllers;


import mvca.Controller;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public abstract  class ClickController extends Controller implements MouseListener{

    
    @Override
    public void mouseClicked(MouseEvent me) {
      if(isValid())
          actualizar();
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }


    protected abstract boolean isValid() ;


}

