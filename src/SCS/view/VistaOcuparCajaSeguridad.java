package SCS.view;





import SCS.Controller.OcuparCajaSeguridadController;
import SCS.Controller.TarifasController;
import  SCS.model.CajaDeSeguridad;
import  SCS.model.Tarifa;
import  SCS.model.TarifaTiempo;
import mvca.Model;
import mvca.View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class VistaOcuparCajaSeguridad extends JFrame implements View{
    private JLabel lblRates;
    private JComboBox cbxHours;
    private JTextField txtRates;
    private JButton btnFirt;
    private JButton btnLast; 
    
    public VistaOcuparCajaSeguridad(){
        //tamaño --
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);//---cambiar JFRame.HIDE_ON_CLOSE
        centerFrame();
        iniciarComponentes();
        
    }
    public void centerFrame(){
        Toolkit myScreen = Toolkit.getDefaultToolkit();
    
        Dimension sizeScreen = myScreen.getScreenSize();

        int highScreen =  sizeScreen.height;
        int widthScreen = sizeScreen.width;

        this.setSize(widthScreen/4, highScreen/4);
        this.setLocation(widthScreen/4, highScreen/4);
    }
    
    public void iniciarComponentes(){
        JPanel panel= new JPanel(new BorderLayout());
        
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        JPanel panelButtons= new JPanel(new GridLayout(1,2,5,5));
        panelButtons.add(btnFirt=new JButton("Siguiente"));
        panelButtons.add(btnLast=new JButton("Cancelar"));
        
        JPanel panelC=new JPanel(new GridLayout(1,3,5,5));
       
        panelC.add(lblRates = new JLabel("Tarifa (h:$):"));
        cbxHours=new JComboBox();
        panelC.add(cbxHours);
        txtRates=new JTextField(5);
        panelC.add(txtRates);
        
        this.add(panel);
        panel.add(panelC,BorderLayout.NORTH);
        panel.add(panelButtons,BorderLayout.SOUTH);
    
    }
    
    public int getTiempoTarifa(){
        return Integer.parseInt(cbxHours.getSelectedItem().toString());
    }
    public void setCostoTarifa(int costo){
        this.txtRates.setText(Integer.toString(costo));
    }
    public int getTiempoTarifaSeleccionada(){
        return ((int)this.cbxHours.getSelectedItem());
    }
    public void addPanelCenter(JPanel panelCenter){
        this.add(panelCenter,BorderLayout.CENTER);
    }
    public void setControllerAL(OcuparCajaSeguridadController cOCajaSeguridad) {
        this.btnFirt.addActionListener(cOCajaSeguridad);
    }
    ///????
    public void setControllerIL(TarifasController cTarifas) {
        this.cbxHours.addItemListener(cTarifas);
    }
    public void cargarTarifas(Iterable<Tarifa> tarifas){
        for(Tarifa tarifa:tarifas){
            //mientras se da una solucion
            this.cbxHours.addItem(((TarifaTiempo)tarifa).getHoras());
        }
    }
    
    @Override
    public void actualizar(Model modelo) {
        cargarTarifas(((CajaDeSeguridad)modelo).getTatiras());
        this.setVisible(true);
        System.out.println("Ocupar desocupar Caja de seguridad");
    }

    @Override
    public void notificarMensaje(Model model, String mensaje) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

