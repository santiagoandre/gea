/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SCS.view;

import mvca.Controller;
import mvca.Model;
import mvca.View;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author DD
 */
public class VistaCajaSeguridad extends JPanel implements  View{
    private Image ImageBox;
    private boolean isOpen;
    private VistaDesocuparCajaSeguridad vDCS;
    private VistaOcuparCajaSeguridad vOCS;
    
    public VistaCajaSeguridad(VistaDesocuparCajaSeguridad vDCS, VistaOcuparCajaSeguridad vOCS){
        this.vDCS=vDCS;
        this.vOCS=vOCS;
        this.isOpen=true;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        File miImagen;
        if (isOpen){
            miImagen= new File("resources/Image/ImageBoxClose.jpg");
        }else{
            miImagen= new File("resources/Image/ImageBoxOpen.jpg");
        }
        
       

        try {ImageBox=ImageIO.read(miImagen);
        }catch(IOException e) {
            System.out.println("La imagen no se encontro.");
        }

        g.drawImage(ImageBox, 0, 0, null);
        setOpaque(false);
        
    }
    public void setController(Controller c){
        this.addMouseListener((MouseListener) c);
        
    }

    public VistaDesocuparCajaSeguridad getvDCS() {
        return vDCS;
    }

    public VistaOcuparCajaSeguridad getvOCS() {
        return vOCS;
    }

   

    @Override
    public void actualizar(Model modelo) {
        //hrow new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.isOpen=false;
        paintComponent(this.getGraphics());
        try {
            Thread.sleep(1*1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(VistaCajaSeguridad.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.isOpen=true;
        paintComponent(this.getGraphics());
        //System.out.println("Actualizar ViewBox");
    }

    @Override
    public void notificarMensaje(Model model, String mensaje) {
    	System.out.println(mensaje);
        
    }
 
}

