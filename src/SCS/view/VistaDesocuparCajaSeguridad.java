package SCS.view;


import SCS.Controller.DesocuparCajaSeguridadController;
import  SCS.model.CajaDeSeguridad;
import mvca.Model;
import mvca.View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;


public class VistaDesocuparCajaSeguridad extends JFrame implements View{
    private JLabel lblPassword;
    private JPasswordField txtPassword;
    private JButton btnFirt;
    private JButton btnLast; 
    
    public VistaDesocuparCajaSeguridad(){
        //tamaño , visibilidad
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);//---cambiar JFRame.HIDE_ON_CLOSE
        centerFrame();
        iniciarComponentes();
    }
     //Centrar el marco
    public void centerFrame(){
        Toolkit myScreen = Toolkit.getDefaultToolkit();
    
        Dimension sizeScreen = myScreen.getScreenSize();

        int highScreen =  sizeScreen.height;
        int widthScreen = sizeScreen.width;

        this.setSize(widthScreen/4, highScreen/4);
        this.setLocation(widthScreen/4, highScreen/4);
    }
    public int getClave(){
        return Integer.parseInt(txtPassword.getText());
    }
    private void iniciarComponentes(){
        JPanel panel= new JPanel(new BorderLayout());
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        
        JPanel panelButtons= new JPanel(new GridLayout(1,2,5,5));
        panelButtons.add(btnFirt=new JButton("Retirar"));
        panelButtons.add(btnLast=new JButton("Cancelar"));
        
        
        JPanel panelC= new JPanel(new GridLayout(1,1,5,5));
        panelC.add(lblPassword= new JLabel(""));
        panelC.add(txtPassword= new JPasswordField(10));
        
        panel.add(panelC,BorderLayout.NORTH);
        panel.add(panelButtons,BorderLayout.SOUTH);
        
        this.add(panel);
    }
    
    
    public void addPanelCenter(JPanel panelCenter){
        this.add(panelCenter,BorderLayout.CENTER);
    }
    public void setController(DesocuparCajaSeguridadController cDCajaSeguridad) {
      this.btnFirt.addActionListener(cDCajaSeguridad);
    }
    @Override
    public void actualizar(Model modelo) {
        int estado= ((CajaDeSeguridad)modelo).getEstado();
        switch(estado){
            case CajaDeSeguridad.OCUPADA:
                this.lblPassword.setText("Clave: ");
                System.out.println("Ocupada");
                this.setVisible(true);
                break;
            case CajaDeSeguridad.BLOQUEADA:
                this.lblPassword.setText("Clave del Operador: ");
                System.out.println("BLOUEADA");
                this.setVisible(true);
                break;
        }
    }

    @Override
    public void notificarMensaje(Model model, String mensaje) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

