package SCS.Controller;



import AbsControllers.ButtonController;
import SCS.model.CajaDeSeguridad;
import SCS.model.SistemasCaja.AbsSistemaDesbloqueo;
import SCS.view.VistaDesocuparCajaSeguridad;

import javax.swing.JOptionPane;


public class DesocuparCajaSeguridadController extends ButtonController{
    private CajaDeSeguridad caja;
    private VistaDesocuparCajaSeguridad view;
    
    public DesocuparCajaSeguridadController(CajaDeSeguridad caja, VistaDesocuparCajaSeguridad vista){
        this.caja=caja;
        this.view=vista;
    }
    
    @Override
    public void actualizar() {
        int clave  = view.getClave();
        AbsSistemaDesbloqueo sistema = caja.getSistemaDesbloqueo();
        sistema.proporcionarCodigo(clave);
        sistema.desbloquear();
        //view.setVisible(false);
    }

    @Override
    protected boolean isValid() {
        return true;
    }

  
}

