/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SCS.Controller;

import AbsControllers.ItemChangeController;
import SCS.model.CajaDeSeguridad;
import SCS.view.VistaOcuparCajaSeguridad;

/**
 *
 * @author DD
 */
public class TarifasController extends ItemChangeController {
    private CajaDeSeguridad caja;
    private VistaOcuparCajaSeguridad view;
    
    public TarifasController(CajaDeSeguridad caja, VistaOcuparCajaSeguridad vista){
        this.caja=caja;
        this.view=vista;
    }
    
    @Override
    public void actualizar() {
        System.out.println("ddd");
        int tiempo = view.getTiempoTarifaSeleccionada();
        int costo =caja.getGestorTarifas().getCostoPor(tiempo);
        System.out.println(costo);
        view.setCostoTarifa(costo);
    }
    
    @Override
    protected boolean isValid() {
        return true;
    }

    
}
