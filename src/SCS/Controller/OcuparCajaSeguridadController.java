package SCS.Controller;


import AbsControllers.ButtonController;
import SCS.model.CajaDeSeguridad;
import SCS.model.SistemasCaja.ISistemaBloqueo;
import SCS.view.VistaOcuparCajaSeguridad;

public class OcuparCajaSeguridadController extends ButtonController{
    private CajaDeSeguridad caja;
    private VistaOcuparCajaSeguridad view;
    
    public OcuparCajaSeguridadController(CajaDeSeguridad caja, VistaOcuparCajaSeguridad vista){
        this.caja=caja;
        this.view=vista;
    }
    @Override
    public void actualizar() {
        int t = view.getTiempoTarifa();
        ISistemaBloqueo sistema = caja.getSistemaBloqueo();
        caja.seleccionarTarifa(caja.getGestorTarifas().getTarifaMinima(t));
        sistema.bloquear();
        view.setVisible(false);
    }
    

    @Override
    protected boolean isValid() {
        return view.getTiempoTarifa()>0;
    }
}

