package SCS.Controller;

import AbsControllers.ClickController;
import SCS.model.CajaDeSeguridad;
import SCS.view.VistaCajaSeguridad;
import mvca.View;


public class CajaSeguridadController extends ClickController{
    private CajaDeSeguridad caja;
    private VistaCajaSeguridad view;
    
    public CajaSeguridadController(CajaDeSeguridad caja, VistaCajaSeguridad vista){
        this.caja=caja;
        this.view=vista;
    }
    
    @Override
    public void actualizar() {
        int estado = caja.getEstado();
        View v = null;
        switch(estado){
            case CajaDeSeguridad.OCUPADA:
                v  = view.getvDCS();
                break;
            case CajaDeSeguridad.LIBRE:
                v  = view.getvOCS();  
                view.actualizar(caja);
                break;
            case CajaDeSeguridad.BLOQUEADA:
                v  = view.getvDCS();//Logica diferente
                break; 
        }
        if(v!=null)
            v.actualizar(caja);
            //actualiza--
                
    }

    @Override
    protected boolean isValid() {
        return true;
    }

    
}

