package SCS.model;

import java.util.List;

public abstract class ServicioTemporizado extends Servicio{
	protected int tiempoUso;/*aunque esto ya se puede  ver en tarifa
	 						lo coloco aqui porque no siempre es cierto*/
	public void setTiempoUso(int tiempo){
		this.tiempoUso = tiempo;
	}
	public int getTiempoUso(){
		return this.tiempoUso ;
	}
	private void cobrarTiempoExtra(int tiempoExtra) {//en minutos
		Tarifa tarifaMinima =gestorTarifas.getTarifaMinima(tiempoExtra);
		if (tarifaMinima != null){
			this.seleccionarTarifa(tarifaMinima);
			cobrar();
		}
	}
	
}
