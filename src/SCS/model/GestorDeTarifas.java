package SCS.model;

import java.util.List;


public abstract class GestorDeTarifas{            
    protected List<Tarifa> tarifas;
    public List<Tarifa> getTarifas() {
        return tarifas;
    }
    public abstract void cargarTarifas ();
	public int getCostoPor(int tiempo) {
		for(Tarifa t: tarifas){
			TarifaTiempo tt = (TarifaTiempo) t;
			if(tiempo==tt.getHoras())
				return tt.getHoras();
		
		}
		return -1;
	}
	public Tarifa getTarifaMinima(int tiempo){
		
		for(Tarifa t: tarifas){
			TarifaTiempo tt = (TarifaTiempo) t;
			if(tiempo<=tt.getHoras())
				return tt;
		
		}
		return null;
	}
}

