package SCS.model;


public abstract class Tarifa{ 
    private int costo;
    
    public Tarifa(int costo){
        this.costo=costo;
    }
    public int getCosto (  )
    {
        return costo;
    }
    
    public void setCosto(int costo){
        this.costo=costo;
    }
}

