package SCS.model.SistemasCaja;

import java.util.Calendar;
import java.util.Date;

import SCS.model.CajaDeSeguridad;

public class SistemaDesbloqueoOperador  extends AbsSistemaDesbloqueo{
	private CajaDeSeguridad caja;
	private int codigoDesbloqueo=122;
	private int codigoProporcionado;
	private SistemaBloqueoTiempoExcedido sbte;
	private AbsSistemaDesbloqueo sistemaDesbloqueo; 
	public SistemaDesbloqueoOperador(CajaDeSeguridad cajaDeSeguridad) {
		this.caja = cajaDeSeguridad;
	}
	
	@Override
	public void desbloquear() {
		if(codigoProporcionado==codigoDesbloqueo){
			seExcedio();
			abrir();
		}else{
			caja.notificar("Error: clave de operardor erronea");
		}
		
		
	}
	public void abrir(){
    	caja.setEstado(caja.LIBRE);
    	caja.setTiempoUso(-1);
    	caja.seleccionarTarifa(null);
    	caja.notificar("Se ha liberado la caja.");
    	
    }
	public void proporcionarCodigo(int codigo){
		this.codigoProporcionado = codigo;
	}
	private boolean seExcedio() {
		Date fechaFin =sumarHorasFecha(caja.getFechaInicio(),caja.getTiempoUso());
		Date fechaActual =  new Date();
		int horasExcedidas =(int) ((fechaActual.getTime()-fechaFin.getTime())/1000)/60;
		if(horasExcedidas>0){
			caja.setSistemaBloqueo(sbte);
			this.sbte.setTiempoExcedido(horasExcedidas);
			this.sbte.bloquear();
			return true;
		}
		return false;
	}
	public Date sumarHorasFecha(Date fecha, int horas){
	       Calendar calendar = Calendar.getInstance();


	       calendar.setTime(fecha); // Configuramos la fecha que se recibe

	       calendar.add(Calendar.HOUR, horas);  // numero de d�as a a�adir, o restar en caso de d�as<0

	       return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos

	  }

	public void setSBTE(SistemaBloqueoTiempoExcedido sbte) {
		this.sbte = sbte;
		
	}

	public void setSD(SistemaDesbloqueo sd) {
		this.sistemaDesbloqueo = sd;
		
	}

	@Override
	public int getCodigoDesbloqueoUsuario() {
		return sistemaDesbloqueo.getCodigoDesbloqueoUsuario();
	}

	@Override
	public void setCodigoDesbloqueoUsuario(int codigo) {
		
		 sistemaDesbloqueo.setCodigoDesbloqueoUsuario(codigo);
	}
	
}
