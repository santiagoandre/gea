package SCS.model.SistemasCaja;

import java.util.Calendar;
import java.util.Date;

import SCS.model.CajaDeSeguridad;

public class SistemaDesbloqueo  extends AbsSistemaDesbloqueo{
	private CajaDeSeguridad caja;
	private int intentos;
	private AbsSistemaDesbloqueo sistemaDesbloqueoOperador;
	private SistemaBloqueoTiempoExcedido sbte; 
	
	public SistemaDesbloqueo(CajaDeSeguridad cajaDeSeguridad) {
		this.caja =cajaDeSeguridad;
	}
	public void setCodigoDesbloqueo(int codigo){
		this.codigoDesbloqueo = codigo;
		intentos= 0;
	}
	public void setSBTE(SistemaBloqueoTiempoExcedido sbte){
		this.sbte = sbte; 
	}
	public void setSDO(SistemaDesbloqueoOperador sdo){
		this.sistemaDesbloqueoOperador = sdo;
	}
	@Override
	public void desbloquear() {
		
		if(codigoProporcionado==codigoDesbloqueo){
			if(!seExcedio()){
				abrir();
				return;
			}
		}
		intentos++;
		if(intentos == 3){
			caja.setEstado(caja.BLOQUEADA);
			caja.setSistemaDesbloqueo(sistemaDesbloqueoOperador);
			caja.notificar("Ha excedido el numero de untentos para abrir la caja\nPor favor llame al operador");
		}else
			caja.notificar("Clave invalida, intentelo de nuevo");
	}
	public void proporcionarCodigo(int codigo){
		this.codigoProporcionado = codigo;
	}
    public void abrir(){
    	caja.setEstado(caja.LIBRE);
    	caja.setTiempoUso(-1);
    	caja.seleccionarTarifa(null);
    	caja.notificar("Se ha liberado la caja.");
    	
    }
	
	private boolean seExcedio() {
		Date fechaFin =sumarHorasFecha(caja.getFechaInicio(),caja.getTiempoUso());
		
		Date fechaActual =  new Date();
		int horasExcedidas =(int) ((fechaActual.getTime()-fechaFin.getTime())/1000)/60;
		if(horasExcedidas>0){
			this.sbte.setTiempoExcedido(horasExcedidas);
			caja.setSistemaBloqueo(sbte);
			this.sbte.bloquear();
			return true;
		}
		return false;
	}
	public Date sumarHorasFecha(Date fecha, int horas){
	       Calendar calendar = Calendar.getInstance();


	       calendar.setTime(fecha); // Configuramos la fecha que se recibe

	       calendar.add(Calendar.HOUR, horas);  // numero de d�as a a�adir, o restar en caso de d�as<0

	       return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos

	  }
	@Override
	public int getCodigoDesbloqueoUsuario() {
		return codigoDesbloqueo;
		
	}
	@Override
	public void setCodigoDesbloqueoUsuario(int codigo) {
		codigoDesbloqueo = codigo;
		
	}
	
}
