package SCS.model.SistemasCaja;

import java.util.Date;

import SCS.model.CajaDeSeguridad;
import SCS.model.TarifaTiempo;
import mvca.Model;

//clase que hace un bloqueo cuando la cajja esta desbloqueada
public class SistemaBloqueo  implements ISistemaBloqueo{
	private CajaDeSeguridad caja;
	private SistemaDesbloqueo sistemaDesbloqueo;
	public SistemaBloqueo(CajaDeSeguridad caja){
		this.caja = caja;
	}
	@Override
	public void bloquear() {
		this.caja.cobrar();
	}
	@Override
	public void pagoExistoso() {
		TarifaTiempo tarifa =(TarifaTiempo) caja.getTarifaSeleccionada();
		
		
		int codigo = generarCodigoDesbloqueo();
		bloquear(codigo);
	}
	private void bloquear(int codigoBloqueo){
		caja.setEstado(caja.OCUPADA);
		caja.setFechaInicio(new Date());
		TarifaTiempo tarifa = (TarifaTiempo) caja.getTarifaSeleccionada();
		sistemaDesbloqueo.setCodigoDesbloqueo(codigoBloqueo);
		caja.setTiempoUso(tarifa.getHoras());//ServicioTemporizado
		caja.notificar("Codigo desbloqueo: "+ codigoBloqueo);
		caja.notificar();
	}
	 public int generarCodigoDesbloqueo(){
	        return (int) (Math.random() * 999) + 1000;
	 }
	 public void setSistemaDesbloqueo(SistemaDesbloqueo s){
		 this.sistemaDesbloqueo = s;
	 }

}
