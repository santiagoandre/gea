package SCS.model.SistemasCaja;



import SCS.model.CajaDeSeguridad;
import SCS.model.TarifaTiempo;

public class SistemaBloqueoTiempoExcedido implements ISistemaBloqueo{
	private CajaDeSeguridad caja;
	private int tiempoExcedido;
	private ISistemaBloqueo sistemaBloqueo;
	public SistemaBloqueoTiempoExcedido(CajaDeSeguridad caja){
		
		this.caja = caja;
	}
	public void setTiempoExcedido(int minutos){
		this.tiempoExcedido = minutos;
	}
	public void setSD(SistemaBloqueo sb) {
		this.sistemaBloqueo = sb;
		
	}
	@Override
	public void bloquear() {
		caja.notificar("Alerta: Se ha excedido "+ tiempoExcedido+" horas, tiene que pagarlas");
		this.caja.cobrar();
	}
	@Override
	public void pagoExistoso() {
		TarifaTiempo tarifa = (TarifaTiempo) caja.getTarifaSeleccionada();
		this.caja.setTiempoUso(tarifa.getHoras()-tiempoExcedido);
		caja.setEstado(caja.OCUPADA);
		caja.setSistemaBloqueo(sistemaBloqueo);
	}
	

}
