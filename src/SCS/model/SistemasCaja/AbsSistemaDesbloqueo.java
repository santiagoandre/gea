package SCS.model.SistemasCaja;

public abstract class  AbsSistemaDesbloqueo{

	protected int codigoDesbloqueo;
	protected int codigoProporcionado;
	public abstract  void desbloquear();
	public void  proporcionarCodigo(int codigo){
		codigoProporcionado = codigo;
	}
	public abstract int getCodigoDesbloqueoUsuario() ;
	public abstract void setCodigoDesbloqueoUsuario(int codigoDesbloqueo2);
		
	
	
}
