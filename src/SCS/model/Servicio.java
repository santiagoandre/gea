package SCS.model;


import java.util.Date;

import java.util.List;

import SF.IFactura;
import mvca.Model;


public abstract class Servicio extends Model implements SPA.API.IClienteSistemaPago{

    private List tarifas;
    private SPA.API.ICobrable sistemaPago;
    private int estado;
    //informacion del servicio cuando lo estan usando
    private  Date startTime;
    protected Tarifa tarifaSeleccionada;
	protected GestorDeTarifas gestorTarifas;
    
    public Date getFechaInicio(){
    	return startTime;
    }
    public int getEstado(){
    	return estado;
    }
    public List getTatiras(){
    	return tarifas;
    }
    public GestorDeTarifas getGestorTarifas(){
    	return this.gestorTarifas;
    }
    public Tarifa getTarifaSeleccionada(){
    	return this.tarifaSeleccionada;
    }
    public void setEstado(int nuevoEstado){
    	this.estado = nuevoEstado;
    	this.notificar();
    }
    public void setFechaInicio(Date fecha){
    	this.startTime = fecha;
    }
    public void cobrar(){
    	if(tarifaSeleccionada == null)
    		return;
    	int costo =tarifaSeleccionada.getCosto();
    	SPA.API.ICobrable sisPago = fabricaSisPago();
    	sisPago.cobrar(costo,this);
    	
    }
    
    public void cargarTarifas(){
    	this.gestorTarifas = fabricaGestorTarifas();
    	
    	this.tarifas = gestorTarifas.getTarifas();
    }
    
    public void seleccionarTarifa(Tarifa t){
    	tarifaSeleccionada = t;
    }
    protected abstract GestorDeTarifas fabricaGestorTarifas();
    protected abstract SPA.API.ICobrable fabricaSisPago();
    protected abstract void generarFactura(String[] datos);
   
}

