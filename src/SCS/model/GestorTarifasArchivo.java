package SCS.model;

import java.util.ArrayList;


public class GestorTarifasArchivo extends GestorDeTarifas{
    
    public GestorTarifasArchivo(){
        this.tarifas= new ArrayList<Tarifa>();
        cargarTarifas();
    }

    
    
    @Override
    public void cargarTarifas() {
        //leer desde archivo
        tarifas.add(new TarifaTiempo(1,3));
        tarifas.add(new TarifaTiempo(2,6));
        tarifas.add(new TarifaTiempo(12,30));
        tarifas.add(new TarifaTiempo(24,50));
    }

}

