package SCS.model;


public class TarifaTiempo extends Tarifa{

	private int horas;
    
    public TarifaTiempo(int horas,int costo){
        super(costo);
        this.horas=horas;
    }
    
    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
}

