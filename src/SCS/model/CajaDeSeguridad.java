package SCS.model;






import SCS.model.SistemasCaja.AbsSistemaDesbloqueo;
import SCS.model.SistemasCaja.ISistemaBloqueo;
import SCS.model.SistemasCaja.SistemaBloqueo;
import SCS.model.SistemasCaja.SistemaBloqueoTiempoExcedido;
import SCS.model.SistemasCaja.SistemaDesbloqueo;
import SCS.model.SistemasCaja.SistemaDesbloqueoOperador;
import SF.GeneradorFacturaCS;
import SF.IFactura;





public class CajaDeSeguridad extends ServicioTemporizado{
    public static final int OCUPADA=1;
    public static final int LIBRE=2;
    public static final int BLOQUEADA=3;

    private ISistemaBloqueo sistemaBloqueo;
    private AbsSistemaDesbloqueo sistemaDesbloqueo;
    
    private String id;
    public CajaDeSeguridad(String id, int estado){
    	this.id = id;
    	this.setEstado(estado);
    	cargarSistemas();
    	cargarTarifas();
    }
    private void cargarSistemas(){
    	SistemaBloqueo sb = new SistemaBloqueo(this);
    	SistemaBloqueoTiempoExcedido sbte =  new SistemaBloqueoTiempoExcedido(this); 
    	SistemaDesbloqueo sd = new SistemaDesbloqueo(this);
    	SistemaDesbloqueoOperador sdo = new SistemaDesbloqueoOperador(this);
    	//sbte.setSB(sistemaBloqueo);
    	sb.setSistemaDesbloqueo(sd);
    	sd.setSDO(sdo);
    	sdo.setSD(sd);
    	sd.setSBTE(sbte);
    	sdo.setSBTE(sbte);
    	sbte.setSD(sb);
		this.sistemaBloqueo= sb;
    	if(getEstado()== BLOQUEADA)
    		this.sistemaDesbloqueo = sdo;
    	else
    		this.sistemaDesbloqueo = sd;
    }


	@Override
	public void notificar(SPA.API.ICobrable c) {//el sistema de pago notifica que ya termino
		if(c.isSuccess()) {//se realizo el pago
			sistemaBloqueo.pagoExistoso();
			this.generarFactura(obtenerDatos());
			//le dice al sistema de bloque que hubo un pago exitoso
			//para que este continue con el bloqueo del sistema
			}//sino no se hace nada
	}
	private String[] obtenerDatos() {
		String[] datos = new String[4];
		datos[0]=this.id;
		datos[1]=Integer.toString(this.sistemaDesbloqueo.getCodigoDesbloqueoUsuario());
		datos[2]=Integer.toString(this.tiempoUso);
		datos[3]=Integer.toString(this.tarifaSeleccionada.getCosto());
		return datos;
	}
	public void setSistemaBloqueo(ISistemaBloqueo sistema){
		this.sistemaBloqueo = sistema;
	}
	public void setSistemaDesbloqueo(AbsSistemaDesbloqueo sistema){
		this.sistemaDesbloqueo = sistema;
	}
	public ISistemaBloqueo getSistemaBloqueo(){
		return this.sistemaBloqueo ;
	}
	public AbsSistemaDesbloqueo getSistemaDesbloqueo(){
		return this.sistemaDesbloqueo;
	}
	@Override
	protected GestorDeTarifas fabricaGestorTarifas() {
		return new GestorTarifasArchivo();//"baseTarifas/CajaSeguridad");
	}
	@Override
	protected SPA.API.ICobrable fabricaSisPago() {
		SPA.API.FabricaSPArchivo f= new SPA.API.FabricaSPArchivo();
		return f.getInstance();
	}
	public String getId() {
		return id;
	}
	@Override
	public void generarFactura(String[] datos) {
		IFactura generador= GeneradorFacturaCS.obtenerInstancia();
		generador.crearFactura(datos);
		
	}

	

    
   
}

